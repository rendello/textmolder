#!/usr/bin/python3.6

from collections import deque
import re
import asyncio
import discord
from discord.ext import commands

from text_commands import text_commands

from secret import client_secret


bot = commands.Bot(command_prefix="!tm")

bot_commands = {}

for tc in text_commands:
    for alias in tc["aliases"]:
        bot_commands[alias] = tc["command"]



# ----- Functions

def chunk_text(text, max_size):
    ''' Splits long text into a list of strings with a max size.

    Args:
        text (str): A potentially long string to split.
        max_size (int): How long each string can be before split.

    Returns:
        list[str]: The original string split at the requested length.
    '''

    return [text[i:i+max_size] for i in range(0, len(text), max_size)]


async def apply_text_funcs(potential_text):
    text_funcs = potential_text.split("|")
    text_funcs = [tf.strip() for tf in text_funcs]

    text = text_funcs.pop(0)

    for index, tf in enumerate(text_funcs):
        text = bot_commands[tf](text)

    # Discord messages capped at 2000 chars, may need multiple messages
    return chunk_text(text, max_size=2000)



# ----- Events

@bot.event
async def on_ready():
    await bot.change_presence(
        activity=discord.Game("!tm help | text modifications with pipes!")
    )


@bot.event
async def on_message(message):
    if '|' in message.content and message.author.id != bot.user.id:
        text = message.content

        groups = re.findall(r'.*?\{(.+?)\}', text)
        for group in groups:
            text = text.replace("{" + group + "}", await apply_text_funcs(group))

        if text.startswith('|'):
            async for m in message.channel.history(limit=2):
                text = m.content + text

        text_chunks = await apply_text_funcs(text)

        for index, tc in enumerate(text_chunks):
            if index < 3:
                await message.channel.send(tc)
                await asyncio.sleep(2)
            else:
                await message.channel.send("Ok, message way too long :P")
                break


if __name__ == "__main__":
    bot.run(client_secret)
