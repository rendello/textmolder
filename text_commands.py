#!/usr/bin/python3.6

import text_functions as tf

text_commands = (
    {
        "aliases": ["caps", "uppercase", "upper", "majiscule"],
        "command": tf.uppercase,
        "category": "basic",
        "help": "Uppercases text.",
        "example": {
            "input": "Hello, world! | upper",
            "output": "HELLO, WORLD!",
        },
    },
    {
        "aliases": ["lowercase", "lower", "miniscule"],
        "command": tf.lowercase,
        "category": "basic",
        "help": "Lowercases text.",
        "example": {
            "input": "Hello, WORLD! | lower",
            "output": "hello, world!",
        },
    },
    {
        "aliases": ["swapcase", "swap case", "swap"],
        "command": tf.swapcase,
        "category": "basic",
        "help": "Swaps the case of the text. Also see `mock`.",
        "example": {
            "input": "Hello, WORLD! | swapcase",
            "output": "hELLO, world!",
        },
    },
    {
        "aliases": ["clap", "clapback"],
        "command": tf.clap,
        "category": "misc",
        "help": "Uppercases text and separates words with clapping emojis.",
        "example": {
            "input": "you are valid and so is this communication style | clap",
            "output": "YOU 👏 ARE 👏 VALID 👏 AND 👏 SO 👏 IS 👏 THIS 👏 COMMUNICATION 👏 STYLE",
        },
    },
    {
        "aliases": ["mock", "spongebob"],
        "command": tf.mock,
        "category": "misc",
        "help": "Randomly upper- and lowercases letters in text.",
        "examples": {
            "input": "This is a good thing. | mock",
            "output": "ThiS iS a GooD tHinG.",
        },
    },
    {
        "aliases": ["zalgo", "spooky", "hecomes"],
        "command": tf.zalgo,
        "category": "misc",
        "help": "Adds random combining characters to make text look crazy.",
        "examples": {"input": "todo", "output": "todo",},
    },
    {
        "aliases": ["anagram"],
        "command": tf.anagram,
        "category": "misc",
        "help": "Scrambles all characters in their respective words.",
        "examples": {"input": "todo", "output": "todo",},
    },
    {
        "aliases": ["redact", "consor", "expunge"],
        "command": tf.redact,
        "category": "substitutions",
        "help": "Replaces letters and numbers with a black block character.",
        "examples": {
            "input": "It's essential that you know [...]! | redact",
            "output": "████ █████████ ████ ███ ████ [...]!",
        },
    },
    {
        "aliases": ["vaporwave", "vapour", "vapor", "vapourwave", "fullwidth", "full"],
        "command": tf.vapourwave,
        "category": "substitutions",
        "help": "Changes letters to their full-width equivalents.",
        "examples": {"input": "nice AESTHETICC | full", "output": "ｎｉｃｅ ＡＥＳＴＨＥＴＩＣＣ",},
    },
    {
        "aliases": ["leet", "haxxor", "hacker", "1337"],
        "command": tf.leet,
        "category": "substitutions",
        "help": "Changes letters to their leet equivalents.",
        "examples": {
            "input": "Mess with the best, die like the rest. | leet",
            "output": "M3$$ WI7H 7H3 83$7, DI3 1IK3 7H3 R3$7.",
        },
    },
    {
        "aliases": ["blackletter", "gothic", "fraktur", "old"],
        "command": tf.light_blackletter,
        "category": "substitutions",
        "help": "Changes letters to their old-timey equivalents.",
        "examples": {
            "input": "This is soooo legible | blackletter",
            "output": "𝔗𝔥𝔦𝔰 𝔦𝔰 𝔰𝔬𝔬𝔬𝔬 𝔩𝔢𝔤𝔦𝔟𝔩𝔢",
        },
    },
    {
        "aliases": ["blackletter!", "gothic!", "fraktur!", "old!"],
        "command": tf.heavy_blackletter,
        "category": "substitutions",
        "help": "Changes letters to their bold old-timey equivalents.",
        "examples": {
            "input": "This is even more legible | blackletter!",
            "output": "𝕿𝖍𝖎𝖘 𝖎𝖘 𝖊𝖛𝖊𝖓 𝖒𝖔𝖗𝖊 𝖑𝖊𝖌𝖎𝖇𝖑𝖊",
        },
    },
    {
        "aliases": ["fronch", "fake french", '"french"'],
        "command": tf.fake_french,
        "category": "substitutions",
        "help": "“Translates” to “french”",
        "examples": {"input": "todo", "output": "todo",},
    },
    {
        "aliases": ["md5"],
        "command": tf.md5,
        "category": "hashing",
        "help": "Creates an MD5 hash from text.",
        "examples": {
            "input": "12345goodPa$$word | md5",
            "output": "b735c2f4ffa92fd7ed887bc423a7b3fd",
        },
    },
    {
        "aliases": ["hash", "sha256"],
        "command": tf.sha256,
        "category": "hashing",
        "help": "Creates an SHA256 hash from text.",
        "examples": {
            "input": "12345goodPa$$word | hash",
            "output": "4b89ce08630dc13be855658e9152d47dd1762800f7825d010018e02d3a67c6ae",
        },
    },
    {
        "aliases": ["hex", "hexidecimal", "0x"],
        "command": tf.hexidecimal,
        "category": "",
        "help": "Gets the hex of the text.",
        "examples": {"input": "", "output": "",},
    },
    {
        "aliases": ["bin", "binary"],
        "command": tf.binary,
        "category": "",
        "help": "Gets the binary of the text.",
        "examples": {"input": "", "output": "",},
    },
)
